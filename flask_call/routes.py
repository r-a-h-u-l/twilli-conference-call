import os
import json
import secrets
import subprocess
import datetime
from twilio.rest import Client
from datetime import date
from twilio.twiml.voice_response import VoiceResponse, Dial
from flask_call import app
from flask import jsonify, request, url_for

"""
Creates an endpoint that can be used in your TwiML App as the Voice Request Url.
In order to make an outgoing call using Twilio Voice SDK, you need to provide a
TwiML App SID in the Access Token. You can run your server, make it publicly
accessible and use `/makeCall` endpoint as the Voice Request Url in your TwiML App.
"""
account_sid = "ACed55b3f2fb4e093edc2e1d7535f4e4c9"
auth_token = "db3c2da4bebc9e4fd8731570f96132bd"
client = Client(account_sid, auth_token)
sessionID_to_callsid = {}
sessionID_to_confsid = {}
session_id = secrets.token_hex(16)


@app.route("/makeCall", methods=["GET", "POST"])
def makeCall():
    print("## Making a call")
    resp = VoiceResponse()
    to_id = "+917016637366"
    from_id = "+12013576653"

    # creates the call from the from_id to the to_id, adding the to_id to the conference
    call = client.calls.create(
        from_=from_id,
        to=to_id,
        url="http://1b0b2192a790.ngrok.io" + "/joinConf/" + str(session_id),
        status_callback_event=["completed"],
        status_callback="http://1b0b2192a790.ngrok.io"
        + "/completeCall/"
        + str(session_id),
    )
    # updates global dictionary to handle edge case where caller leaves before callee picks up
    global sessionID_to_callsid
    sessionID_to_callsid[session_id] = call.sid

    # dials the caller (from_id) into the conference
    dial = Dial()
    dial.conference(
        session_id,
        status_callback="http://1b0b2192a790.ngrok.io" + "/leave",
        status_callback_event="leave join",
    )
    resp.append(dial)
    return str(resp)


# this is an endpoint to add the callee into the conference call
@app.route("/joinConf/<string:call_session_id>", methods=["GET", "POST"])
def conferenceCall(call_session_id):
    print("## Making a conference call")
    resp = VoiceResponse()
    dial = Dial()
    dial.conference(
        call_session_id,
        waitUrl="https://handler.twilio.com/twiml/EH856eb47cd8e27725056f899e96074b12",
        # waitUrl="http://twimlets.com/holdmusic?Bucket=com.twilio.music.classical",
        status_callback="http://1b0b2192a790.ngrok.io" + "/leave",
        status_callback_event=["leave"],
        record=True,
        wait_method=addUser(),
    )
    resp.append(dial)
    return str(resp)


@app.route("/addUser", methods=["GET", "POST"])
def addUser():
    global session_id
    phone_number = "+919574714884"
    print("Attemtping to add phone number to call: " + phone_number)

    participant = client.conferences(session_id).participants.create(
        from_="+12013576653",
        to=phone_number,
        conference_status_callback="http://1b0b2192a790.ngrok.io" + "/leave",
        conference_status_callback_event=["leave"],
    )

    data = {
        "status_code": 200,
    }
    resp = jsonify(data)
    return resp


# this endpoint is called whenever a participant leaves a conference call
# used to end call depending on participants left and to handle edge case of original caller # leaves the call before callee picks up
# also called when the original caller first joins the conference to get map the sessionID # to the conference_sid to handle the edge case when the callee rejects call
@app.route("/leave", methods=["GET", "POST"])
def leaveCall():
    try:
        request.values["RecordingUrl"]
        url = request.values["RecordingUrl"]
        filename = (
            "+12013576653_to_"
            + client.calls.list(limit=20)[0].to
            + ":"
            + str(datetime.datetime.now())
            + ".mp3"
        )
        file_location = os.path.join(
            "/Users/iblinfotech/Desktop/twilio_call/calls/", filename
        )
        subprocess.run(
            ["curl", url, "--output", file_location],
            capture_output=True,
        )
    except:
        print("No RecordingUrl found")
    event = request.values["SequenceNumber"]
    conference_sid = request.values["ConferenceSid"]
    global sessionID_to_confsid
    sessionID_to_confsid[request.values["FriendlyName"]] = conference_sid

    if request.values["StatusCallbackEvent"] == "participant-leave":
        print("A Participant Left Call")
        # ends conference call if only 1 participant left
        participants = client.conferences(conference_sid).participants
        if len(participants.list()) == 1:
            client.conferences(conference_sid).update(status="completed")
            print("Call ended")
        # ends conference call if original caller leaves before callee picks up
        elif len(participants.list()) == 0 and event == "2":
            client.calls(sessionID_to_callsid[request.values["FriendlyName"]]).update(
                status="completed"
            )
            print("Call ended")

    data = {
        "status_code": 200,
    }
    resp = jsonify(data)
    return resp


# this is an endpoint to end the conference call if the callee rejects the call
@app.route("/completeCall/<string:call_session_id>", methods=["GET", "POST"])
def completeCall(call_session_id):
    print("## Ending conference call, callee rejected call")
    global sessionID_to_confsid
    participants = client.conferences(
        sessionID_to_confsid[call_session_id]
    ).participants

    # only does so if 1 participant left in the conference call (i.e. the caller)
    if len(participants.list()) == 1:
        client.conferences(sessionID_to_confsid[call_session_id]).update(
            status="completed"
        )
        print("Call ended")
    data = {
        "status_code": 200,
    }

    resp = jsonify(data)
    # recordings = client.recordings.list(limit=20)
    # recording = recordings[0].sid
    # filename = (
    #     "+12013576653_to_"
    #     + client.calls.list(limit=20)[0].to
    #     + ":"
    #     + str(datetime.datetime.now())
    #     + ".mp3"
    # )
    # file_location = os.path.join(
    #     "/Users/iblinfotech/Desktop/twilio_call/calls/", filename
    # )
    # url = f"https://api.twilio.com/2010-04-01/Accounts/ACed55b3f2fb4e093edc2e1d7535f4e4c9/Recordings/{recording}"
    # subprocess.run(
    #     ["curl", url, "--output", file_location],
    #     capture_output=True,
    # )

    return resp
