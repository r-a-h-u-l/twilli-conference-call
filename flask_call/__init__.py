import os
from flask import Flask


app = Flask(__name__)
app.config["SECRET_KEY"] = "8fd45afb79df01907b51e81501b80667"
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///site.db"


from flask_call import routes