import requests

BASE = "http://127.0.0.1:5000/"
data = [
    {
        "likes": 10,
        "name": "rahul",
        "views": 5000,
    },
    {
        "likes": 101,
        "name": "viren",
        "views": 50400,
    },
    {
        "likes": 1001,
        "name": "nirmal",
        "views": 505400,
    },
]

for i in range(len(data)):
    response = requests.put(BASE + "video/" + str(i), data[i])
    print(response.json())

input()
response = requests.get(BASE + "video/2")
print(response.json())