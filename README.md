# <h1 align="center">Twilio Call Integration Using Flask</h1>

<!-- ABOUT THE PROJECT -->
## About The Project
 
A simple Twilio call Integration using Python-Flask Framework. Twilio call is dial to the main user and when main user pick the call then a conference call is made to the Destination user. the whole conversation is recorded and the recording will be stored in Project Folder.
 
### Built With
 
* [Python](https://www.python.org/)
* [Flask](https://flask.palletsprojects.com/en/1.1.x/)
* [Twilio](https://www.twilio.com/)
 
### Installation
 
1. Download or clone the Repository to your device
2. Create Virtual Environment using `python3 -m venv <your environment name>`
3. Activate Virtual Environment for Mac and Linux user `source <virtual env path>/bin/activate`
4. Activate Virtual Environment for Windows user `venv\Scripts\activate`
5. type `pip install -r requirements.txt` (this will install required package for project)
6. type `python3 run.py`
